#!/bin/sh

if [ ! -f /factorio/saves/save.zip ]; then
    /factorio/bin/x64/factorio --create /factorio/saves/save.zip
fi

/factorio/bin/x64/factorio --start-server-load-latest --server-settings /factorio/data/server-settings.json
