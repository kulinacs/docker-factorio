FROM ubuntu:latest
ARG FACTORIO_VERSION="0.16.38"

RUN apt-get update && apt-get install -y \
    curl \
    xz-utils \
 && rm -rf /var/lib/apt/lists/*

RUN curl -L https://www.factorio.com/get-download/${FACTORIO_VERSION}/headless/linux64 -o factorio.tar.xz && tar xf factorio.tar.xz && rm factorio.tar.xz

WORKDIR /factorio

RUN mkdir saves

COPY factorio-entrypoint.sh .

EXPOSE 34197/udp

VOLUME ["/factorio/saves/"]

ENTRYPOINT ["/factorio/factorio-entrypoint.sh"]
